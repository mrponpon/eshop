package userUsecases

import (
	"fmt"

	"gitlab.com/mrponpon/eshop/config"
	"gitlab.com/mrponpon/eshop/modules/users"
	"gitlab.com/mrponpon/eshop/modules/users/userRepositories"
	"gitlab.com/mrponpon/eshop/pkg/auth"
	"golang.org/x/crypto/bcrypt"
)

type IUserusecase interface {
	InsertCustomer(req *users.UserRegisterReq) (*users.UserPassport, error)
	InsertAdmin(req *users.UserRegisterReq) (*users.UserPassport, error)
	GetPassport(req *users.UserCredential) (*users.UserPassport, error)
	RefeshPassport(req *users.UserRefreshCredential) (*users.UserPassport, error)
	DeleteOauth(oauth *users.UserRemoveCredential) error
	GetUserProfile(userId string) (*users.User, error)
}

type userUsecase struct {
	cfg            config.Iconfig
	userRepository userRepositories.IUserRepository
}

func Userusecase(cfg config.Iconfig, userRepository userRepositories.IUserRepository) IUserusecase {
	return &userUsecase{
		cfg:            cfg,
		userRepository: userRepository,
	}
}

func (u *userUsecase) InsertCustomer(req *users.UserRegisterReq) (*users.UserPassport, error) {
	//Hash password
	if err := req.BcryptHashing(); err != nil {
		return nil, err
	}
	//Insert user
	result, err := u.userRepository.InsertUser(req, false)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (u *userUsecase) InsertAdmin(req *users.UserRegisterReq) (*users.UserPassport, error) {
	//Hash password
	if err := req.BcryptHashing(); err != nil {
		return nil, err
	}
	//Insert user
	result, err := u.userRepository.InsertUser(req, true)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (u *userUsecase) GetPassport(req *users.UserCredential) (*users.UserPassport, error) {
	//Find user
	user, err := u.userRepository.FindOneUserByEmail(req.Email)
	if err != nil {
		return nil, err
	}
	//Compare password
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password)); err != nil {
		return nil, fmt.Errorf("password is invalid")
	}
	//sign token
	accessToken, err := auth.NewAuth(auth.Access, u.cfg.Jwt(), &users.UserClaims{
		Id:     user.Id,
		RoleId: user.RoleId,
	})
	refreshToken, err := auth.NewAuth(auth.Refesh, u.cfg.Jwt(), &users.UserClaims{
		Id:     user.Id,
		RoleId: user.RoleId,
	})
	//Set passport
	passport := &users.UserPassport{
		User: &users.User{
			Id:       user.Id,
			Email:    user.Email,
			Username: user.Username,
			RoleId:   user.RoleId,
		},
		Token: &users.UserToken{
			AccessToken:  accessToken.SignToken(),
			RefreshToken: refreshToken.SignToken(),
		},
	}
	err = u.userRepository.InsertOauth(passport)
	if err != nil {
		return nil, err
	}
	return passport, nil
}

func (u *userUsecase) RefeshPassport(req *users.UserRefreshCredential) (*users.UserPassport, error) {
	//parses token
	claims, err := auth.ParseToken(u.cfg.Jwt(), req.RefreshToken)
	if err != nil {
		return nil, err
	}
	//check oauth
	oauth, err := u.userRepository.FindOneOauth(req.RefreshToken)
	if err != nil {
		return nil, err
	}
	//find profile
	profile, err := u.userRepository.GetProfile(oauth.UserId)
	if err != nil {
		return nil, err
	}
	newClaims := &users.UserClaims{
		Id:     profile.Id,
		RoleId: profile.RoleId,
	}
	accessToken, err := auth.NewAuth(
		auth.Access,
		u.cfg.Jwt(),
		newClaims,
	)
	if err != nil {
		return nil, err
	}
	refresh_token := auth.RepeatToken(
		u.cfg.Jwt(),
		newClaims,
		claims.ExpiresAt.Unix(),
	)
	passport := &users.UserPassport{
		User: profile,
		Token: &users.UserToken{
			Id:           oauth.Id,
			AccessToken:  accessToken.SignToken(),
			RefreshToken: refresh_token,
		},
	}
	err = u.userRepository.UpdateOauth(passport.Token)
	if err != nil {
		return nil, err
	}
	return passport, nil
}

func (u *userUsecase) DeleteOauth(oauth *users.UserRemoveCredential) error {
	err := u.userRepository.DeleteOauth(oauth)
	if err != nil {
		return err
	}

	return nil
}

func (u *userUsecase) GetUserProfile(userId string) (*users.User, error) {
	profile, err := u.userRepository.GetProfile(userId)
	if err != nil {
		return nil, err
	}
	return profile, nil
}
