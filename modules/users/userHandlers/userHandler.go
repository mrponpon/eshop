package userHandlers

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/mrponpon/eshop/config"
	"gitlab.com/mrponpon/eshop/modules/entities"
	"gitlab.com/mrponpon/eshop/modules/users"
	"gitlab.com/mrponpon/eshop/modules/users/userUsecases"
	"gitlab.com/mrponpon/eshop/pkg/auth"
)

type userHandlerErrCode string

const (
	signUpCustomerErr     userHandlerErrCode = "users-001"
	signInErr             userHandlerErrCode = "users-002"
	refreshErr            userHandlerErrCode = "users-003"
	signOutErr            userHandlerErrCode = "users-004"
	signUpAdminErr        userHandlerErrCode = "users-005"
	generateAdminTokenErr userHandlerErrCode = "users-006"
	GetUserProfileErr     userHandlerErrCode = "users-007"
)

type admintoken struct {
	Token string `json:"token"`
}

type IUserhandler interface {
	SignUpCustomer(c *fiber.Ctx) error
	SignIn(c *fiber.Ctx) error
	RefeshPassport(c *fiber.Ctx) error
	SignOut(c *fiber.Ctx) error
	SignUpAdmin(c *fiber.Ctx) error
	GenerateAdminToken(c *fiber.Ctx) error
	GetUserProfile(c *fiber.Ctx) error
}

type userHandler struct {
	cfg          config.Iconfig
	userUsecases userUsecases.IUserusecase
}

func UserHandler(cfg config.Iconfig, userUsecases userUsecases.IUserusecase) IUserhandler {
	return &userHandler{
		cfg:          cfg,
		userUsecases: userUsecases,
	}
}
func (h *userHandler) SignUpCustomer(c *fiber.Ctx) error {
	//requesst body parser
	req := new(users.UserRegisterReq)
	if err := c.BodyParser(req); err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(signUpCustomerErr),
			err.Error(),
		).Res()
	}
	//Email validation
	if !req.IsEmail() {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(signUpCustomerErr),
			"email pattern is invalid",
		).Res()
	}
	//Insert
	result, err := h.userUsecases.InsertCustomer(req)
	if err != nil {
		switch err.Error() {
		case "username has been used":
			return entities.NewResponse(c).Error(
				fiber.ErrBadRequest.Code,
				string(signUpCustomerErr),
				err.Error(),
			).Res()
		case "email has been used":
			return entities.NewResponse(c).Error(
				fiber.ErrBadRequest.Code,
				string(signUpCustomerErr),
				err.Error(),
			).Res()
		default:
			return entities.NewResponse(c).Error(
				fiber.ErrInternalServerError.Code,
				string(signUpCustomerErr),
				err.Error(),
			).Res()
		}
	}
	return entities.NewResponse(c).Success(fiber.StatusCreated, result).Res()
}

func (h *userHandler) SignUpAdmin(c *fiber.Ctx) error {
	//requesst body parser
	req := new(users.UserRegisterReq)
	if err := c.BodyParser(req); err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(signUpCustomerErr),
			err.Error(),
		).Res()
	}
	//Email validation
	if !req.IsEmail() {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(signUpCustomerErr),
			"email pattern is invalid",
		).Res()
	}
	//Insert
	result, err := h.userUsecases.InsertCustomer(req)
	if err != nil {
		switch err.Error() {
		case "username has been used":
			return entities.NewResponse(c).Error(
				fiber.ErrBadRequest.Code,
				string(signUpCustomerErr),
				err.Error(),
			).Res()
		case "email has been used":
			return entities.NewResponse(c).Error(
				fiber.ErrBadRequest.Code,
				string(signUpCustomerErr),
				err.Error(),
			).Res()
		default:
			return entities.NewResponse(c).Error(
				fiber.ErrInternalServerError.Code,
				string(signUpCustomerErr),
				err.Error(),
			).Res()
		}
	}
	return entities.NewResponse(c).Success(fiber.StatusCreated, result).Res()
}
func (h *userHandler) SignIn(c *fiber.Ctx) error {
	req := new(users.UserCredential)
	err := c.BodyParser(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(signInErr),
			err.Error(),
		).Res()
	}
	passport, err := h.userUsecases.GetPassport(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(signInErr),
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(fiber.StatusOK, passport).Res()
}

func (h *userHandler) RefeshPassport(c *fiber.Ctx) error {
	req := new(users.UserRefreshCredential)
	err := c.BodyParser(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(refreshErr),
			err.Error(),
		).Res()
	}
	passport, err := h.userUsecases.RefeshPassport(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(refreshErr),
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(fiber.StatusOK, passport).Res()
}

func (h *userHandler) SignOut(c *fiber.Ctx) error {
	req := new(users.UserRemoveCredential)
	err := c.BodyParser(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(signOutErr),
			err.Error(),
		).Res()
	}
	err = h.userUsecases.DeleteOauth(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(signOutErr),
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(fiber.StatusOK, req.OauthId).Res()
}

func (h *userHandler) GenerateAdminToken(c *fiber.Ctx) error {
	adminToken, err := auth.NewAuth(auth.Admin, h.cfg.Jwt(), nil)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			string(generateAdminTokenErr),
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(
		fiber.StatusOK,
		&admintoken{
			Token: adminToken.SignToken(),
		},
	).Res()
}

func (h *userHandler) GetUserProfile(c *fiber.Ctx) error {
	//set params
	userId := strings.Trim(c.Params("user_id"), " ")
	//get profile
	result, err := h.userUsecases.GetUserProfile(userId)
	if err != nil {
		switch err.Error() {
		case "get user failed: sql: no rows in result set":
			return entities.NewResponse(c).Error(
				fiber.ErrBadRequest.Code,
				string(GetUserProfileErr),
				err.Error(),
			).Res()
		default:
			return entities.NewResponse(c).Error(
				fiber.ErrInternalServerError.Code,
				string(GetUserProfileErr),
				err.Error(),
			).Res()
		}
	}
	return entities.NewResponse(c).Success(fiber.StatusOK, result).Res()
}
