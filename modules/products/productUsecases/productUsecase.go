package productusecases

import (
	"math"

	"gitlab.com/mrponpon/eshop/modules/products"
	productrepositories "gitlab.com/mrponpon/eshop/modules/products/productRepositories"
)

type IProductUsecase interface {
	FindOneProduct(productId string) (*products.Product, error)
	FindManyProduct(req *products.ProductFilter) *products.PaginateResponse
	InsertProduct(req *products.Product) (*products.Product, error)
	UpdateProduct(req *products.Product) (*products.Product, error)
	DeleteProduct(productId string) error
}

type productusecase struct {
	productrepository productrepositories.IProductRepository
}

func ProductUsecase(productrepository productrepositories.IProductRepository) IProductUsecase {
	return &productusecase{
		productrepository: productrepository,
	}
}

func (u *productusecase) FindOneProduct(productId string) (*products.Product, error) {
	product, err := u.productrepository.FileOneProduct(productId)
	if err != nil {
		return nil, err
	}
	return product, nil
}

func (u *productusecase) FindManyProduct(req *products.ProductFilter) *products.PaginateResponse {
	productData, count := u.productrepository.FindManyProduct(req)
	return &products.PaginateResponse{
		Data:      productData,
		Page:      req.Page,
		Limit:     req.Limit,
		TotalPage: int(math.Ceil(float64(count) / float64(req.Limit))),
		TotalItem: count,
	}
}

func (u *productusecase) InsertProduct(req *products.Product) (*products.Product, error) {
	product, err := u.productrepository.InsertProduct(req)
	if err != nil {
		return nil, err
	}
	return product, nil
}

func (u *productusecase) UpdateProduct(req *products.Product) (*products.Product, error) {
	product, err := u.productrepository.UpdateProduct(req)
	if err != nil {
		return nil, err
	}
	return product, nil
}

func (u *productusecase) DeleteProduct(productId string) error {
	err := u.productrepository.DeleteProduct(productId)
	if err != nil {
		return err
	}
	return nil
}
