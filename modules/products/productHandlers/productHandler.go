package producthandlers

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/mrponpon/eshop/config"
	"gitlab.com/mrponpon/eshop/modules/appinfo"
	"gitlab.com/mrponpon/eshop/modules/entities"
	"gitlab.com/mrponpon/eshop/modules/files"
	filesrepositories "gitlab.com/mrponpon/eshop/modules/files/filesRepositories"
	"gitlab.com/mrponpon/eshop/modules/products"
	productusecases "gitlab.com/mrponpon/eshop/modules/products/productUsecases"
)

type productsHandlerErrCode string

const (
	findOneProductErr  productsHandlerErrCode = "product-001"
	findManyProductErr productsHandlerErrCode = "product-002"
	insertProductErr   productsHandlerErrCode = "product-003"
	updateProductErr   productsHandlerErrCode = "product-004"
	deleteProductErr   productsHandlerErrCode = "product-005"
)

type IProductHandler interface {
	FindOneProduct(c *fiber.Ctx) error
	FindManyProduct(c *fiber.Ctx) error
	InsertProduct(c *fiber.Ctx) error
	UpdateProduct(c *fiber.Ctx) error
	DeleteProduct(c *fiber.Ctx) error
}
type producthandler struct {
	cfg            config.Iconfig
	productusecase productusecases.IProductUsecase
	fileRepository filesrepositories.IFilesRepository
}

func ProductHanler(cfg config.Iconfig, productusecase productusecases.IProductUsecase, fileRepository filesrepositories.IFilesRepository) IProductHandler {
	return &producthandler{
		cfg:            cfg,
		productusecase: productusecase,
		fileRepository: fileRepository,
	}
}

func (h *producthandler) FindOneProduct(c *fiber.Ctx) error {
	productId := strings.Trim(c.Params("product_id"), " ")
	product, err := h.productusecase.FindOneProduct(productId)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			string(findOneProductErr),
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(fiber.StatusOK, product).Res()
}

func (h *producthandler) FindManyProduct(c *fiber.Ctx) error {
	req := &products.ProductFilter{
		PaginationReq: &products.PaginationReq{},
		SortReq:       &products.SortReq{},
	}
	err := c.QueryParser(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(findManyProductErr),
			err.Error(),
		).Res()
	}
	if req.Page < 1 {
		req.Page = 1
	}
	if req.Limit < 5 {
		req.Limit = 5
	}
	if req.OrderBy == "" {
		req.OrderBy = "title"
	}
	if req.Sort == "" {
		req.Sort = "ASC"
	}
	productData := h.productusecase.FindManyProduct(req)
	return entities.NewResponse(c).Success(fiber.StatusOK, productData).Res()

}

func (h *producthandler) InsertProduct(c *fiber.Ctx) error {
	req := &products.Product{
		Category: &appinfo.Category{},
		Image:    make([]*products.Image, 0),
	}
	err := c.BodyParser(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(insertProductErr),
			err.Error(),
		).Res()
	}
	if req.Category.Id <= 0 {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(insertProductErr),
			"category id is invalid",
		).Res()
	}
	product, err := h.productusecase.InsertProduct(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			string(insertProductErr),
			err.Error(),
		).Res()
	}

	return entities.NewResponse(c).Success(fiber.StatusCreated, product).Res()

}

func (h *producthandler) UpdateProduct(c *fiber.Ctx) error {
	productId := strings.Trim(c.Params("product_id"), " ")
	req := &products.Product{
		Id:       productId,
		Category: &appinfo.Category{},
		Image:    make([]*products.Image, 0),
	}
	err := c.BodyParser(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(updateProductErr),
			err.Error(),
		).Res()
	}
	product, err := h.productusecase.UpdateProduct(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			string(updateProductErr),
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(fiber.StatusOK, product).Res()
}

func (h *producthandler) DeleteProduct(c *fiber.Ctx) error {
	productId := strings.Trim(c.Params("product_id"), " ")
	product, err := h.productusecase.FindOneProduct(productId)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			string(deleteProductErr),
			err.Error(),
		).Res()
	}
	destinations := make([]*files.DeleteFileReq, 0)
	for _, img := range product.Image {
		splitstring := strings.Split(img.Url, "images")
		des := &files.DeleteFileReq{
			Destination: "images" + splitstring[1],
		}
		destinations = append(destinations, des)
	}
	if err := h.productusecase.DeleteProduct(productId); err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			string(deleteProductErr),
			err.Error(),
		).Res()
	}
	if err := h.fileRepository.DeleteFileOnGcp(destinations); err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			string(deleteProductErr),
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(fiber.StatusOK, productId).Res()
}
