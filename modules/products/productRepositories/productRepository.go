package productrepositories

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/mrponpon/eshop/config"
	filesrepositories "gitlab.com/mrponpon/eshop/modules/files/filesRepositories"
	"gitlab.com/mrponpon/eshop/modules/products"
	productspatterns "gitlab.com/mrponpon/eshop/modules/products/productsPatterns"
)

type IProductRepository interface {
	FileOneProduct(productId string) (*products.Product, error)
	FindManyProduct(req *products.ProductFilter) ([]*products.Product, int)
	InsertProduct(req *products.Product) (*products.Product, error)
	UpdateProduct(req *products.Product) (*products.Product, error)
	DeleteProduct(productId string) error
}

type productrepository struct {
	db             *sqlx.DB
	cfg            config.Iconfig
	fileRepository filesrepositories.IFilesRepository
}

func ProductRespository(db *sqlx.DB, cfg config.Iconfig, fileRepository filesrepositories.IFilesRepository) IProductRepository {
	return &productrepository{
		db:             db,
		cfg:            cfg,
		fileRepository: fileRepository,
	}
}

func (r *productrepository) FileOneProduct(productId string) (*products.Product, error) {
	query := `
	SELECT 
		to_jsonb("t")
		FROM (
			SELECT
			"p"."id",
			"p"."title",
			"p"."description",
			"p"."price",
			(
				SELECT 
					to_jsonb("ct")
				FROM (
					SELECT 
						"c"."id",
						"c"."title"
					FROM "categories" "c"
						LEFT JOIN "products_categories" "pc" ON "pc"."category_id" = "c"."id"
					WHERE "pc"."product_id" = "p"."id"
				) AS "ct"
			) AS "category",
			"p"."created_at",
			"p"."updated_at",
			(
				SELECT 
					COALESCE(array_to_json(array_agg("it")),'[]'::json)
				FROM (
					SELECT
						"i"."id",
						"i"."filename",
						"i"."url"
					FROM "images" "i"
					WHERE "i"."product_id" = "p"."id"
				) AS "it"
			) AS "images"
			FROM "products" "p"
			WHERE "p"."id" = $1
			LIMIT 1
		) AS "t";`
	productBytes := make([]byte, 0)
	product := &products.Product{
		Image: make([]*products.Image, 0),
	}
	err := r.db.Get(&productBytes, query, productId)
	if err != nil {
		return nil, fmt.Errorf("get product failed:%v", err)

	}
	err = json.Unmarshal(productBytes, &product)
	if err != nil {
		return nil, fmt.Errorf("unmarshal product failed: %v", err)

	}

	return product, nil
}

func (r *productrepository) FindManyProduct(req *products.ProductFilter) ([]*products.Product, int) {
	builder := productspatterns.FindProductBuilder(r.db, req)
	result := builder.FindProduct().Result()
	count := builder.CountProduct().Count()
	builder.FindProduct().PrintQuery()
	return result, count

}

func (r *productrepository) InsertProduct(req *products.Product) (*products.Product, error) {
	builder := productspatterns.InsertProductBuilder(r.db, req)
	productId, err := builder.InsertProduct()
	if err != nil {
		return nil, err
	}
	product, err := r.FileOneProduct(productId)
	if err != nil {
		return nil, err
	}
	return product, nil
}

func (r *productrepository) UpdateProduct(req *products.Product) (*products.Product, error) {
	_, err := r.FileOneProduct(req.Id)
	if err != nil {
		return nil, err
	}
	builder := productspatterns.UpdateProductBuilder(r.db, req, r.fileRepository)
	err = builder.UpdateProduct()
	if err != nil {
		return nil, err
	}
	product, err := r.FileOneProduct(req.Id)
	if err != nil {
		return nil, err
	}
	return product, nil
}

func (r *productrepository) DeleteProduct(productId string) error {
	query := `DELETE FROM "products" WHERE "id" = $1;`
	if _, err := r.db.ExecContext(context.Background(), query, productId); err != nil {
		return fmt.Errorf("delete product failed: %v", err)
	}
	return nil
}
