package products

import (
	"gitlab.com/mrponpon/eshop/modules/appinfo"
)

type Product struct {
	Id          string            `json:"id"`
	Title       string            `json:"title"`
	Description string            `json:"description"`
	Category    *appinfo.Category `json:"category"`
	CreateAt    string            `json:"created_at"`
	UpdateAt    string            `json:"updated_at"`
	Price       float64           `json:"price"`
	Image       []*Image          `json:"images"`
}

type Image struct {
	Id       string `db:"id" json:"id"`
	FileName string `db:"filename" json:"filename"`
	Url      string `db:"url" json:"url"`
}

type ProductFilter struct {
	Id     string `query:"id"`
	Search string `query:"search"`
	*PaginationReq
	*SortReq
}

type PaginationReq struct {
	Page      int `query:"page"`
	Limit     int `query:"limit"`
	TotalPage int `query:"total_page"`
	TotalItem int `query:"total_item"`
}

type SortReq struct {
	OrderBy string `query:"order_by"`
	Sort    string `query:"sort"`
}

type PaginateResponse struct {
	Data      any `json:"data"`
	Page      int `json:"page"`
	Limit     int `json:"limit"`
	TotalPage int `json:"total_page"`
	TotalItem int `json:"total_item"`
}
