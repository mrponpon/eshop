package ordersusecase

import (
	"fmt"
	"math"

	"gitlab.com/mrponpon/eshop/modules/orders"
	ordersrepository "gitlab.com/mrponpon/eshop/modules/orders/ordersRepository"
	productrepositories "gitlab.com/mrponpon/eshop/modules/products/productRepositories"
	"gitlab.com/mrponpon/eshop/pkg/utils"
)

type IOrdersUsecase interface {
	FindOneOrder(orderId string) (*orders.Order, error)
	FindOrder(req *orders.OrderFilter) (*orders.PaginateRes, error)
	UpdateOrder(req *orders.Order) (*orders.Order, error)
}

type ordersusecase struct {
	ordersrepository  ordersrepository.IOrdersRepo
	productrepository productrepositories.IProductRepository
}

func OrdersUsecase(ordersrepository ordersrepository.IOrdersRepo, productrepository productrepositories.IProductRepository) IOrdersUsecase {
	return &ordersusecase{
		ordersrepository:  ordersrepository,
		productrepository: productrepository,
	}
}

func (u *ordersusecase) FindOneOrder(orderId string) (*orders.Order, error) {
	result_orders, err := u.ordersrepository.FindOneOrder(orderId)
	if err != nil {
		return nil, err
	}
	return result_orders, nil
}

func (u *ordersusecase) FindOrder(req *orders.OrderFilter) (*orders.PaginateRes, error) {
	result_orders, err := u.ordersrepository.FindOrder(req)
	if err != nil {
		return nil, err
	}
	count, err := u.ordersrepository.CountOrder(req)
	if err != nil {
		return nil, err
	}

	return &orders.PaginateRes{
		Data:      result_orders,
		Page:      req.PaginationReq.Page,
		Limit:     req.PaginationReq.Limit,
		TotalItem: count,
		TotalPage: int(math.Ceil(float64(count) / float64(req.PaginationReq.Limit))),
	}, nil
}

func (u *ordersusecase) UpdateOrder(req *orders.Order) (*orders.Order, error) {
	// Check if products is exists
	for i := range req.Products {
		if req.Products[i].Product == nil {
			return nil, fmt.Errorf("product is nil")
		}

		prod, err := u.productrepository.FileOneProduct(req.Products[i].Product.Id)
		if err != nil {
			return nil, err
		}
		utils.Debug(prod)

		// Set price
		req.TotalPaid += req.Products[i].Product.Price * float64(req.Products[i].Qty)
		req.Products[i].Product = prod
	}
	if err := u.ordersrepository.InitTransaction(); err != nil {
		return nil, err
	}
	if err := u.ordersrepository.InsertOrder(req); err != nil {
		return nil, err
	}
	if err := u.ordersrepository.InsertProductsOrder(req); err != nil {
		return nil, err
	}
	if err := u.ordersrepository.Commit(); err != nil {
		return nil, err
	}
	order, err := u.ordersrepository.FindOneOrder(req.Id)
	if err != nil {
		return nil, err
	}
	return order, nil
}
