package ordershandler

import (
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/mrponpon/eshop/config"
	"gitlab.com/mrponpon/eshop/modules/entities"
	"gitlab.com/mrponpon/eshop/modules/orders"
	ordersusecase "gitlab.com/mrponpon/eshop/modules/orders/ordersUsecase"
)

type orderHandlerErrCode string

const (
	FindOneOrderErr orderHandlerErrCode = "order-001"
	FindOrderErr    orderHandlerErrCode = "order-002"
)

type IOrdersHandler interface {
	FindOneOrder(c *fiber.Ctx) error
	FindOrder(c *fiber.Ctx) error
}

type ordershandler struct {
	cfg           config.Iconfig
	ordersusecase ordersusecase.IOrdersUsecase
}

func OrdersHandler(ordersusecase ordersusecase.IOrdersUsecase, cfg config.Iconfig) IOrdersHandler {
	return &ordershandler{
		cfg:           cfg,
		ordersusecase: ordersusecase,
	}
}

func (h *ordershandler) FindOneOrder(c *fiber.Ctx) error {
	orderId := strings.Trim(c.Params("order_id"), " ")
	result_orders, err := h.ordersusecase.FindOneOrder(orderId)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			string(FindOneOrderErr),
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(fiber.StatusOK, result_orders).Res()
}

func (h *ordershandler) FindOrder(c *fiber.Ctx) error {
	req := &orders.OrderFilter{
		SortReq:       &orders.SortReq{},
		PaginationReq: &orders.PaginationReq{},
	}
	if err := c.QueryParser(req); err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			string(FindOrderErr),
			err.Error(),
		).Res()
	}
	if req.PaginationReq.Page < 1 {
		req.PaginationReq.Page = 1
	}
	if req.PaginationReq.Limit < 5 {
		req.PaginationReq.Limit = 5
	}
	orderByMap := map[string]string{
		"id":        `"o"."id"`,
		"create_at": `"o"."created_at"`,
	}
	if orderByMap[req.SortReq.OrderBy] == "" {
		req.SortReq.OrderBy = orderByMap["id"]
	}
	req.SortReq.Sort = strings.ToUpper(req.SortReq.Sort)
	sortMap := map[string]string{
		"DESC": "DESC",
		"ASC":  "ASC",
	}
	if sortMap[req.SortReq.Sort] == "" {
		req.SortReq.Sort = sortMap["DESC"]
	}
	if req.StartDate != "" {
		start, err := time.Parse("2006-01-02", req.StartDate)
		if err != nil {
			return entities.NewResponse(c).Error(
				fiber.ErrBadRequest.Code,
				string(FindOrderErr),
				"start date is invalid",
			).Res()
		}
		req.StartDate = start.Format("2006-01-02")
	}
	if req.EndDate != "" {
		end, err := time.Parse("2006-01-02", req.EndDate)
		if err != nil {
			return entities.NewResponse(c).Error(
				fiber.ErrBadRequest.Code,
				string(FindOrderErr),
				"end date is invalid",
			).Res()
		}
		req.EndDate = end.Format("2006-01-02")
	}
	result_orders, err := h.ordersusecase.FindOrder(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			string(FindOrderErr),
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(fiber.StatusOK, result_orders).Res()
}
