package ordersrepository

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/mrponpon/eshop/modules/orders"
)

type IOrdersRepo interface {
	FindOneOrder(orderId string) (*orders.Order, error)
	FindOrder(req *orders.OrderFilter) ([]*orders.Order, error)
	CountOrder(req *orders.OrderFilter) (int, error)
	InitTransaction() error
	InsertOrder(req *orders.Order) error
	InsertProductsOrder(req *orders.Order) error
	Commit() error
}

type ordersrepository struct {
	db *sqlx.DB
	tx *sqlx.Tx
}

func OrderRepository(db *sqlx.DB) IOrdersRepo {
	return &ordersrepository{
		db: db,
	}
}
func (r *ordersrepository) GetTotalPaid(orderId string) (float64, error) {
	query_total_paid := `
	SELECT
		SUM(COALESCE(("po"."product"->>'price')::FLOAT*("po"."qty")::FLOAT, 0))
	FROM "products_orders" "po"
	WHERE "po"."order_id" = $1;`
	var total_paid float64
	err := r.db.Get(&total_paid, query_total_paid, orderId)
	if err != nil {
		return 0, fmt.Errorf("sum error :%v", err)
	}
	return total_paid, nil
}

func (r *ordersrepository) GetProductOrder(orderId string) ([]*orders.ProductsOrder, error) {
	query_product_order := `
	SELECT 
		COALESCE(array_to_json(array_agg("t")),'[]'::json)
		FROM (
			SELECT 
			"po"."id",
			"po"."qty",
			"po"."product"
			FROM "products_orders" "po"	
			WHERE "po"."order_id" = $1
		) AS "t";`
	products_orders := make([]*orders.ProductsOrder, 0)
	products_ordersBytes := make([]byte, 0)
	err := r.db.Get(&products_ordersBytes, query_product_order, orderId)
	if err != nil {
		return nil, fmt.Errorf("product not found :%v", err)
	}
	err = json.Unmarshal(products_ordersBytes, &products_orders)
	if err != nil {
		return nil, fmt.Errorf("unmarshal product failed: %v", err)

	}
	return products_orders, nil
}
func (r *ordersrepository) FindOneOrder(orderId string) (*orders.Order, error) {
	query_orders := `
	SELECT 
		to_jsonb("t")
		FROM (
			SELECT 
			"o"."id",
			"o"."user_id",
			"o"."contact",
			"o"."address",
			"o"."status",
			"o"."transfer_slip",
			"o"."created_at",
			"o"."updated_at"
			FROM "orders" "o"
			WHERE "o"."id"=$1
		) AS "t";`
	result_orders := &orders.Order{
		TransferSlip: &orders.TransferSlip{},
		Products:     make([]*orders.ProductsOrder, 0),
	}
	ordersBytes := make([]byte, 0)

	err := r.db.Get(&ordersBytes, query_orders, orderId)
	if err != nil {
		return nil, fmt.Errorf("order not found :%v", err)
	}
	err = json.Unmarshal(ordersBytes, &result_orders)
	if err != nil {
		return nil, fmt.Errorf("unmarshal product failed: %v", err)

	}
	total_paid, err := r.GetTotalPaid(orderId)
	if err != nil {
		return nil, err
	}
	result_orders.TotalPaid = total_paid
	products_orders, err := r.GetProductOrder(orderId)
	if err != nil {
		return nil, err
	}
	result_orders.Products = products_orders
	return result_orders, nil
}

func (r *ordersrepository) FindOrder(req *orders.OrderFilter) ([]*orders.Order, error) {
	var lastIndex = 0
	values := make([]any, 0)
	query := `
	SELECT
		array_to_json(array_agg("at"))
	FROM (
		SELECT
			"o"."id",
			"o"."user_id",
			"o"."transfer_slip",
			"o"."status",
			(
				SELECT
					array_to_json(array_agg("pt"))
				FROM (
					SELECT
						"spo"."id",
						"spo"."qty",
						"spo"."product"
					FROM "products_orders" "spo"
					WHERE "spo"."order_id" = "o"."id"
				) AS "pt"
			) AS "products",
			"o"."address",
			"o"."contact",
			(
				SELECT
					SUM(COALESCE(("po"."product"->>'price')::FLOAT*("po"."qty")::FLOAT, 0))
				FROM "products_orders" "po"
				WHERE "po"."order_id" = "o"."id"
			) AS "total_paid",
			"o"."created_at",
			"o"."updated_at"
		FROM "orders" "o"
		WHERE 1 = 1`
	//query search
	if req.Search != "" {
		values = append(
			values,
			"%"+strings.ToLower(req.Search)+"%",
			"%"+strings.ToLower(req.Search)+"%",
			"%"+strings.ToLower(req.Search)+"%",
		)
		temp_query := fmt.Sprintf(`
		AND (
			LOWER("o"."user_id") LIKE $%d OR
			LOWER("o"."address") LIKE $%d OR
			LOWER("o"."contact") LIKE $%d
		)`,
			lastIndex+1,
			lastIndex+2,
			lastIndex+3,
		)
		query += temp_query
		lastIndex = len(values)
	}
	//query status
	if req.Status != "" {
		values = append(
			values,
			strings.ToLower(req.Status),
		)
		temp_query := fmt.Sprintf(`
		AND "o"."status" = $%d`,
			lastIndex+1,
		)
		query += temp_query
		lastIndex = len(values)
	}
	//query date
	if req.StartDate != "" && req.EndDate != "" {
		values = append(
			values,
			req.StartDate,
			req.EndDate,
		)
		temp_query := fmt.Sprintf(`
		AND "o"."created_at" BETWEEN DATE($%d) AND ($%d)::DATE + 1`,
			lastIndex+1,
			lastIndex+2,
		)
		query += temp_query
		lastIndex = len(values)
	}
	//query sort
	values = append(values, req.SortReq.OrderBy)
	query += fmt.Sprintf(`
	ORDER BY $%d %s`, lastIndex+1, req.SortReq.Sort)
	lastIndex = len(values)
	values = append(
		values,
		(req.PaginationReq.Page-1)*req.PaginationReq.Limit,
		req.PaginationReq.Limit,
	)
	//query pagination
	query += fmt.Sprintf(`
	OFFSET $%d LIMIT $%d`, lastIndex+1, lastIndex+2)
	lastIndex = len(values)
	query += `) AS "at"`
	raw := make([]byte, 0)
	err := r.db.Get(&raw, query, values...)
	if err != nil {
		return nil, fmt.Errorf("get orders failed :%v", err)
	}
	ordersData := make([]*orders.Order, 0)
	if err := json.Unmarshal(raw, &ordersData); err != nil {
		return nil, fmt.Errorf("unmarshal orders failed:%v", err)
	}
	return ordersData, nil
}

func (r *ordersrepository) CountOrder(req *orders.OrderFilter) (int, error) {
	var lastIndex = 0
	values := make([]any, 0)
	query := `
		SELECT
			COUNT(*) AS "count"
		FROM "orders" "o"
		WHERE 1 = 1`
	//query search
	if req.Search != "" {
		values = append(
			values,
			"%"+strings.ToLower(req.Search)+"%",
			"%"+strings.ToLower(req.Search)+"%",
			"%"+strings.ToLower(req.Search)+"%",
		)
		temp_query := fmt.Sprintf(`
		AND (
			LOWER("o"."user_id") LIKE $%d OR
			LOWER("o"."address") LIKE $%d OR
			LOWER("o"."contact") LIKE $%d
		)`,
			lastIndex+1,
			lastIndex+2,
			lastIndex+3,
		)
		query += temp_query
		lastIndex = len(values)
	}
	//query status
	if req.Status != "" {
		values = append(
			values,
			strings.ToLower(req.Status),
		)
		temp_query := fmt.Sprintf(`
		AND "o"."status" = $%d`,
			lastIndex+1,
		)
		query += temp_query
		lastIndex = len(values)
	}
	//query date
	if req.StartDate != "" && req.EndDate != "" {
		values = append(
			values,
			req.StartDate,
			req.EndDate,
		)
		temp_query := fmt.Sprintf(`
		AND "o"."created_at" BETWEEN DATE($%d) AND ($%d)::DATE + 1`,
			lastIndex+1,
			lastIndex+2,
		)
		query += temp_query
		lastIndex = len(values)
	}
	var count int
	if err := r.db.Get(&count, query, values...); err != nil {
		return 0, fmt.Errorf("count orders failed :%v", err)
	}

	return count, nil
}

func (r *ordersrepository) InitTransaction() error {
	tx, err := r.db.BeginTxx(context.Background(), nil)
	r.tx = tx
	if err != nil {
		return err
	}
	return nil
}

func (r *ordersrepository) InsertOrder(req *orders.Order) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	query := `
	INSERT INTO "orders" (
		"user_id",
		"contact",
		"address",
		"transfer_slip",
		"status"
	)
	VALUES
	($1, $2, $3, $4, $5)
		RETURNING "id";`
	if err := r.tx.QueryRowxContext(
		ctx,
		query,
		req.UserId,
		req.Contact,
		req.Address,
		req.TransferSlip,
		req.Status,
	).Scan(&req.Id); err != nil {
		r.tx.Rollback()
		return fmt.Errorf("insert order failed: %v", err)
	}

	return nil
}

func (r *ordersrepository) InsertProductsOrder(req *orders.Order) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	query := `
	INSERT INTO "products_orders" (
		"order_id",
		"qty",
		"product"
	)
	VALUES`

	values := make([]any, 0)
	lastIndex := 0
	for i := range req.Products {
		values = append(
			values,
			req.Id,
			req.Products[i].Qty,
			req.Products[i].Product,
		)

		if i != len(req.Products)-1 {
			query += fmt.Sprintf(`
			($%d, $%d, $%d),`, lastIndex+1, lastIndex+2, lastIndex+3)
		} else {
			query += fmt.Sprintf(`
			($%d, $%d, $%d);`, lastIndex+1, lastIndex+2, lastIndex+3)
		}

		lastIndex += 3
	}

	if _, err := r.tx.ExecContext(ctx, query, values...); err != nil {
		r.tx.Rollback()
		return fmt.Errorf("insert products_orders failed: %v", err)
	}
	return nil
}

func (r *ordersrepository) Commit() error {
	if err := r.tx.Commit(); err != nil {
		return err
	}
	return nil
}

func (r *ordersrepository) UpdateOrder(req *orders.Order) error {
	query := `
	UPDATE "orders" SET`

	queryWhereStack := make([]string, 0)
	values := make([]any, 0)
	lastIndex := 1

	if req.Status != "" {
		values = append(values, req.Status)

		queryWhereStack = append(queryWhereStack, fmt.Sprintf(`
		"status" = $%d?`, lastIndex))

		lastIndex++
	}

	if req.TransferSlip != nil {
		values = append(values, req.TransferSlip)

		queryWhereStack = append(queryWhereStack, fmt.Sprintf(`
		"transfer_slip" = $%d?`, lastIndex))

		lastIndex++
	}

	values = append(values, req.Id)

	queryClose := fmt.Sprintf(`
	WHERE "id" = $%d;`, lastIndex)

	for i := range queryWhereStack {
		if i != len(queryWhereStack)-1 {
			query += strings.Replace(queryWhereStack[i], "?", ",", 1)
		} else {
			query += strings.Replace(queryWhereStack[i], "?", "", 1)
		}
	}
	query += queryClose

	if _, err := r.db.ExecContext(context.Background(), query, values...); err != nil {
		return fmt.Errorf("update order failed: %v", err)
	}
	return nil
}
