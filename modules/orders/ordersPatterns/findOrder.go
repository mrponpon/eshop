package orderspatterns

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/mrponpon/eshop/modules/orders"
)

type IFindOrderBuilder interface {
}

type findOrderBuilder struct {
	db        *sqlx.DB
	req       *orders.OrderFilter
	query     string
	value     []any
	lastIndex int
}

func FindOrderBuilder(db *sqlx.DB, req *orders.OrderFilter) IFindOrderBuilder {
	return &findOrderBuilder{
		db:    db,
		req:   req,
		value: make([]any, 0),
	}
}

func (b *findOrderBuilder) FindOrder() []*orders.Order {
	return nil
}
func (b *findOrderBuilder) CountOrder() int {
	return 0
}
