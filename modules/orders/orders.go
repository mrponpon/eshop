package orders

import (
	"gitlab.com/mrponpon/eshop/modules/products"
)

type OrderFilter struct {
	Search        string `query:"search"`
	Status        string `query:"status"`
	StartDate     string `query:"start_date"`
	EndDate       string `query:"end_data"`
	PaginationReq *PaginationReq
	SortReq       *SortReq
}
type PaginationReq struct {
	Page      int `query:"page"`
	Limit     int `query:"limit"`
	TotalPage int `query:"total_page" json:"total_page"`
	TotalItem int `query:"total_item" json:"totla_item"`
}

type PaginateRes struct {
	Data      any `json:"data"`
	Page      int `json:"page"`
	Limit     int `json:"limit"`
	TotalPage int `json:"total_page"`
	TotalItem int `json:"total_item"`
}
type SortReq struct {
	OrderBy string `query:"order_by"`
	Sort    string `query:"sort"` // DESC | ASC
}
type Order struct {
	Id           string           `db:"id" json:"id"`
	UserId       string           `db:"user_id" json:"user_id"`
	TransferSlip *TransferSlip    `db:"transfer_slip" json:"transfer_slip"`
	Products     []*ProductsOrder `db:"product_orders" json:"products_orders"`
	Address      string           `db:"address" json:"address"`
	Contact      string           `db:"contact" json:"contact"`
	Status       string           `db:"status" json:"status"`
	TotalPaid    float64          `db:"total_paid" json:"total_paid"`
	CreatedAt    string           `db:"created_at" json:"created_at"`
	UpdatedAt    string           `db:"updated_at" json:"updated_at"`
}

type TransferSlip struct {
	Id       string `json:"id"`
	FileName string `json:"filename"`
	Url      string `json:"url"`
	CreateAt string `json:"created_at"`
}

type ProductsOrder struct {
	Id      string            `db:"id" json:"id"`
	Qty     int               `db:"qty" json:"qty"`
	Product *products.Product `db:"product" json:"product"`
}
