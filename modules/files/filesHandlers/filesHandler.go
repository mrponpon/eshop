package fileshandlers

import (
	"fmt"
	"path/filepath"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/mrponpon/eshop/config"
	"gitlab.com/mrponpon/eshop/modules/entities"
	"gitlab.com/mrponpon/eshop/modules/files"
	filesusecases "gitlab.com/mrponpon/eshop/modules/files/filesUsecases"
	"gitlab.com/mrponpon/eshop/pkg/utils"
)

type fileHandlerErrCoded string

const (
	uploaderr fileHandlerErrCoded = "file-01"
	deleteerr fileHandlerErrCoded = "file-02"
)

type IFileHandler interface {
	UploadFile(c *fiber.Ctx) error
	DeleteFile(c *fiber.Ctx) error
}

type fileHandler struct {
	cfg         config.Iconfig
	fileUsecase filesusecases.IFileUsercase
}

func FileHandler(cfg config.Iconfig, fileUsecase filesusecases.IFileUsercase) IFileHandler {
	return &fileHandler{
		cfg:         cfg,
		fileUsecase: fileUsecase,
	}
}

func (h *fileHandler) UploadFile(c *fiber.Ctx) error {
	req := make([]*files.FileReq, 0)
	form, err := c.MultipartForm()
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(uploaderr),
			err.Error(),
		).Res()
	}
	filesReq := form.File["files"]
	destination := c.FormValue("destination")
	extMap := map[string]string{
		"png":  "png",
		"jpg":  "jpg",
		"jpeg": "jpeg",
	}
	for _, file := range filesReq {
		ext := filepath.Ext(file.Filename)[1:]
		if extMap[ext] != ext || extMap[ext] == "" {
			return entities.NewResponse(c).Error(
				fiber.ErrBadRequest.Code,
				string(uploaderr),
				"extension is unacceptable",
			).Res()
		}
		if file.Size > int64(h.cfg.App().FileLimit()) {
			return entities.NewResponse(c).Error(
				fiber.ErrBadRequest.Code,
				string(uploaderr),
				fmt.Sprintf("files size must less than %d", h.cfg.App().FileLimit()),
			).Res()
		}
		filename := utils.RandFileName(file.Filename)
		req = append(req, &files.FileReq{
			File:        file,
			Destination: destination + "/" + filename,
			FileName:    filename,
			Extension:   ext,
		})
	}
	res, err := h.fileUsecase.UploadToGcp(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			string(uploaderr),
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(fiber.StatusCreated, res).Res()
}

func (h *fileHandler) DeleteFile(c *fiber.Ctx) error {
	req := make([]*files.DeleteFileReq, 0)
	err := c.BodyParser(&req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			string(deleteerr),
			"destination is unacceptable",
		).Res()
	}
	err = h.fileUsecase.DeleteFileOnGcp(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			string(deleteerr),
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(fiber.StatusCreated, nil).Res()
}
