package filesusecases

import (
	"gitlab.com/mrponpon/eshop/modules/files"
	filesrepositories "gitlab.com/mrponpon/eshop/modules/files/filesRepositories"
)

type IFileUsercase interface {
	UploadToGcp(req []*files.FileReq) ([]*files.FileResponse, error)
	DeleteFileOnGcp(req []*files.DeleteFileReq) error
}

type filesUsecase struct {
	filesRepository filesrepositories.IFilesRepository
}

func FileUsecase(filesRepository filesrepositories.IFilesRepository) IFileUsercase {
	return &filesUsecase{
		filesRepository: filesRepository,
	}
}

func (u *filesUsecase) UploadToGcp(req []*files.FileReq) ([]*files.FileResponse, error) {
	res, err := u.filesRepository.UploadToGcp(req)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (u *filesUsecase) DeleteFileOnGcp(req []*files.DeleteFileReq) error {
	err := u.filesRepository.DeleteFileOnGcp(req)
	if err != nil {
		return err
	}
	return nil
}
