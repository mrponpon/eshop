package filesrepositories

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"sync"
	"time"

	"cloud.google.com/go/storage"
	"gitlab.com/mrponpon/eshop/config"
	"gitlab.com/mrponpon/eshop/modules/files"
	"google.golang.org/api/option"
)

type IFilesRepository interface {
	UploadToGcp(req []*files.FileReq) ([]*files.FileResponse, error)
	DeleteFileOnGcp(req []*files.DeleteFileReq) error
}

type filesRepository struct {
	cfg config.Iconfig
}

func FileRepository(cfg config.Iconfig) IFilesRepository {
	return &filesRepository{
		cfg: cfg,
	}
}

type filesPublic struct {
	bucket      string
	destination string
	file        *files.FileResponse
}

func (f *filesPublic) makePublic(ctx context.Context, client *storage.Client) error {
	acl := client.Bucket(f.bucket).Object(f.destination).ACL()
	if err := acl.Set(ctx, storage.AllUsers, storage.RoleReader); err != nil {
		return fmt.Errorf("ACLHandle.Set: %w", err)
	}
	fmt.Printf("Blob %v is now publicly accessible.\n", f.bucket)
	return nil
}
func (r *filesRepository) Uploader(wg *sync.WaitGroup, ctx context.Context, client *storage.Client, jobs <-chan *files.FileReq, result chan<- *files.FileResponse, errs chan<- error) {
	defer wg.Done()
	for job := range jobs {
		container, err := job.File.Open()
		if err != nil {
			errs <- err
			return
		}
		b, err := ioutil.ReadAll(container)
		if err != nil {
			errs <- err
			return
		}
		buf := bytes.NewBuffer(b)

		wc := client.Bucket(r.cfg.App().Gcpbucket()).Object(job.Destination).NewWriter(ctx)

		_, err = io.Copy(wc, buf)
		if err != nil {
			errs <- err
			return
		}
		// Data can continue to be added to the file until the writer is closed.
		err = wc.Close()
		if err != nil {
			errs <- err
			return
		}
		fmt.Printf("%v uploaded to %v.\n", job.FileName, job.Destination)
		newFile := &filesPublic{
			file: &files.FileResponse{
				FileName: job.FileName,
				Url:      fmt.Sprintf("https://storage.googleapis.com/%s/%s", r.cfg.App().Gcpbucket(), job.Destination),
			},
			bucket:      r.cfg.App().Gcpbucket(),
			destination: job.Destination,
		}
		err = newFile.makePublic(ctx, client)
		if err != nil {
			errs <- err
		}
		errs <- nil
		result <- newFile.file
		fmt.Println(result)
	}
}

func (r *filesRepository) UploadToGcp(req []*files.FileReq) ([]*files.FileResponse, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*2)
	defer cancel()
	client, err := storage.NewClient(ctx, option.WithCredentialsFile(r.cfg.App().GoogleCredential()))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	defer client.Close()
	jobsCh := make(chan *files.FileReq, len(req))
	resultCh := make(chan *files.FileResponse, len(req))
	errorCh := make(chan error, len(req))
	defer close(jobsCh)

	wg := new(sync.WaitGroup)
	res := make([]*files.FileResponse, 0)
	for _, r := range req {
		jobsCh <- r
	}
	numworkers := 5
	for i := 0; i < numworkers; i++ {
		wg.Add(1)
		go r.Uploader(wg, ctx, client, jobsCh, resultCh, errorCh)
	}
	go func() {
		wg.Wait()
	}()
	for i := 0; i < len(req); i++ {
		err := <-errorCh
		if err != nil {
			return nil, err
		}
		result := <-resultCh
		res = append(res, result)
	}
	return res, nil
}
func (r *filesRepository) Deleter(ctx context.Context, client *storage.Client, jobs <-chan *files.DeleteFileReq, errs chan<- error) {
	for job := range jobs {
		o := client.Bucket(r.cfg.App().Gcpbucket()).Object(job.Destination)
		attrs, err := o.Attrs(ctx)
		if err != nil {
			errs <- err
			return
		}
		o = o.If(storage.Conditions{GenerationMatch: attrs.Generation})

		err = o.Delete(ctx)
		if err != nil {
			errs <- err
			return
		}
		errs <- nil
	}

}
func (r *filesRepository) DeleteFileOnGcp(req []*files.DeleteFileReq) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	client, err := storage.NewClient(ctx, option.WithCredentialsFile(r.cfg.App().GoogleCredential()))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	defer client.Close()
	jobsCh := make(chan *files.DeleteFileReq, len(req))
	errorCh := make(chan error, len(req))
	defer close(jobsCh)
	for _, r := range req {
		jobsCh <- r
	}
	numworkers := 5
	for i := 0; i < numworkers; i++ {
		go r.Deleter(ctx, client, jobsCh, errorCh)
	}
	for i := 0; i < len(req); i++ {
		err := <-errorCh
		fmt.Println(err)
		if err != nil {
			return err
		}
	}
	return nil
}
