package monitorHandlers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mrponpon/eshop/config"
	"gitlab.com/mrponpon/eshop/modules/entities"
	"gitlab.com/mrponpon/eshop/modules/monitor"
)

type IMonitorHandler interface {
	HealthCheck(c *fiber.Ctx) error
}

type monitorHandler struct {
	cfg config.Iconfig
}

func (h *monitorHandler) HealthCheck(c *fiber.Ctx) error {

	res := &monitor.Monitor{
		Name:    h.cfg.App().Name(),
		Version: h.cfg.App().Version(),
	}
	return entities.NewResponse(c).Success(fiber.StatusOK, res).Res()

}
func MonitorHandler(cfg config.Iconfig) IMonitorHandler {
	return &monitorHandler{
		cfg: cfg,
	}
}
