package servers

import (
	"github.com/gofiber/fiber/v2"
	appinfohandlers "gitlab.com/mrponpon/eshop/modules/appinfo/appinfoHandlers"
	appinforepositories "gitlab.com/mrponpon/eshop/modules/appinfo/appinfoRepositories"
	appinfousecases "gitlab.com/mrponpon/eshop/modules/appinfo/appinfoUsecases"
	fileshandlers "gitlab.com/mrponpon/eshop/modules/files/filesHandlers"
	filesrepositories "gitlab.com/mrponpon/eshop/modules/files/filesRepositories"
	filesusecases "gitlab.com/mrponpon/eshop/modules/files/filesUsecases"
	"gitlab.com/mrponpon/eshop/modules/middlewares/middlewaresHandlers"
	"gitlab.com/mrponpon/eshop/modules/middlewares/middlewaresRepositories"
	"gitlab.com/mrponpon/eshop/modules/middlewares/middlewaresUsecases"
	"gitlab.com/mrponpon/eshop/modules/monitor/monitorHandlers"
	ordershandler "gitlab.com/mrponpon/eshop/modules/orders/ordersHandler"
	ordersrepository "gitlab.com/mrponpon/eshop/modules/orders/ordersRepository"
	ordersusecase "gitlab.com/mrponpon/eshop/modules/orders/ordersUsecase"
	producthandlers "gitlab.com/mrponpon/eshop/modules/products/productHandlers"
	productrepositories "gitlab.com/mrponpon/eshop/modules/products/productRepositories"
	productusecases "gitlab.com/mrponpon/eshop/modules/products/productUsecases"
	"gitlab.com/mrponpon/eshop/modules/users/userHandlers"
	"gitlab.com/mrponpon/eshop/modules/users/userRepositories"
	"gitlab.com/mrponpon/eshop/modules/users/userUsecases"
)

type IModuleFactory interface {
	MonitorModule()
	UserModule()
	AppinfoModule()
	FileModule()
	ProductModule()
	OrderModule()
}

type modulesFactory struct {
	router fiber.Router
	server *server
	mid    middlewaresHandlers.IMiddlewaresHandler
}

func (m *modulesFactory) MonitorModule() {
	handler := monitorHandlers.MonitorHandler(m.server.cfg)
	m.router.Get("/", handler.HealthCheck)
}

func (m *modulesFactory) UserModule() {
	repository := userRepositories.UserRepository(m.server.db)
	usecase := userUsecases.Userusecase(m.server.cfg, repository)
	handler := userHandlers.UserHandler(m.server.cfg, usecase)
	route := m.router.Group("/users")
	route.Post("/signup", m.mid.ApiKeyAuth(), handler.SignUpCustomer)
	route.Post("/signin", m.mid.ApiKeyAuth(), handler.SignIn)
	route.Post("/refresh", m.mid.ApiKeyAuth(), handler.RefeshPassport)
	route.Post("/signout", m.mid.ApiKeyAuth(), handler.SignOut)
	route.Post("/signup-admin", m.mid.ApiKeyAuth(), m.mid.Authorize(2), handler.SignUpAdmin)
	route.Get("/:user_id", m.mid.JwtAuth(), m.mid.ParamsCheck(), handler.GetUserProfile)
	route.Get("/admin/secret", m.mid.JwtAuth(), m.mid.Authorize(2), handler.GenerateAdminToken)
}
func (m *modulesFactory) AppinfoModule() {
	repository := appinforepositories.Appinforepository(m.server.db)
	usecase := appinfousecases.Appinfousecase(repository)
	handler := appinfohandlers.Appinfohandler(m.server.cfg, usecase)
	route := m.router.Group("/appinfo")
	route.Get("/apikey", handler.GenerateApiKey)
	route.Get("/categories", m.mid.ApiKeyAuth(), handler.FindCategory)
	route.Post("/categories", m.mid.JwtAuth(), m.mid.Authorize(2), handler.AddCategory)
	route.Delete("/categories/:category_id", m.mid.JwtAuth(), m.mid.Authorize(2), handler.RemoveCategory)

}

func (m *modulesFactory) FileModule() {
	repository := filesrepositories.FileRepository(m.server.cfg)
	usecase := filesusecases.FileUsecase(repository)
	handler := fileshandlers.FileHandler(m.server.cfg, usecase)
	route := m.router.Group("/files")
	route.Post("/upload", m.mid.JwtAuth(), m.mid.Authorize(2), handler.UploadFile)
	route.Post("/delete", m.mid.JwtAuth(), m.mid.Authorize(2), handler.DeleteFile)
}

func (m *modulesFactory) ProductModule() {
	filerepository := filesrepositories.FileRepository(m.server.cfg)
	repository := productrepositories.ProductRespository(m.server.db, m.server.cfg, filerepository)
	usecase := productusecases.ProductUsecase(repository)
	handler := producthandlers.ProductHanler(m.server.cfg, usecase, filerepository)
	route := m.router.Group("/products")
	route.Get("/", m.mid.ApiKeyAuth(), handler.FindManyProduct)
	route.Get("/:product_id", m.mid.ApiKeyAuth(), handler.FindOneProduct)
	route.Post("/insert", m.mid.JwtAuth(), m.mid.Authorize(2), handler.InsertProduct)
	route.Patch("/:product_id", m.mid.JwtAuth(), m.mid.Authorize(2), handler.UpdateProduct)
	route.Delete("/:product_id", handler.DeleteProduct)
}

func (m *modulesFactory) OrderModule() {
	filerepository := filesrepositories.FileRepository(m.server.cfg)
	productrepository := productrepositories.ProductRespository(m.server.db, m.server.cfg, filerepository)
	repository := ordersrepository.OrderRepository(m.server.db)
	usecase := ordersusecase.OrdersUsecase(repository, productrepository)
	handler := ordershandler.OrdersHandler(usecase, m.server.cfg)
	route := m.router.Group("/orders")
	route.Get("/", m.mid.JwtAuth(), handler.FindOrder)
	route.Get("/:order_id", m.mid.ApiKeyAuth(), handler.FindOneOrder)
}
func InitMiddlewares(s *server) middlewaresHandlers.IMiddlewaresHandler {
	repository := middlewaresRepositories.MiddlewaresRepository(s.db)
	usercase := middlewaresUsecases.MiddlewaresUsecase(repository)
	handler := middlewaresHandlers.MiddlewaresHandler(s.cfg, usercase)
	return handler
}

func NewModule(r fiber.Router, s *server, mid middlewaresHandlers.IMiddlewaresHandler) IModuleFactory {
	return &modulesFactory{
		router: r,
		server: s,
		mid:    mid,
	}
}
