package appinfousecases

import (
	"gitlab.com/mrponpon/eshop/modules/appinfo"
	appinforepositories "gitlab.com/mrponpon/eshop/modules/appinfo/appinfoRepositories"
)

type IAppinfoUsecase interface {
	FindCategory(req *appinfo.CategoryFilter) ([]*appinfo.Category, error)
	InsertCategory(req []*appinfo.Category) error
	DeleteCategory(categoryId int) error
}

type appinfousecase struct {
	appinforepository appinforepositories.IAppinfoRepository
}

func Appinfousecase(appinforepository appinforepositories.IAppinfoRepository) IAppinfoUsecase {
	return &appinfousecase{
		appinforepository: appinforepository,
	}
}

func (u *appinfousecase) FindCategory(req *appinfo.CategoryFilter) ([]*appinfo.Category, error) {
	category, err := u.appinforepository.FindCategory(req)
	if err != nil {
		return nil, err
	}
	return category, nil

}

func (u *appinfousecase) InsertCategory(req []*appinfo.Category) error {
	err := u.appinforepository.InsertCategory(req)
	if err != nil {
		return err
	}

	return nil
}

func (u *appinfousecase) DeleteCategory(categoryId int) error {
	err := u.appinforepository.DeleteCategory(categoryId)
	if err != nil {
		return err
	}
	return nil
}
