package appinforepositories

import (
	"context"
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/mrponpon/eshop/modules/appinfo"
)

type IAppinfoRepository interface {
	FindCategory(req *appinfo.CategoryFilter) ([]*appinfo.Category, error)
	InsertCategory(req []*appinfo.Category) error
	DeleteCategory(categoryId int) error
}

type appinforepository struct {
	db *sqlx.DB
}

func Appinforepository(db *sqlx.DB) IAppinfoRepository {
	return &appinforepository{
		db: db,
	}
}

func (r *appinforepository) FindCategory(req *appinfo.CategoryFilter) ([]*appinfo.Category, error) {
	query := `
	SELECT
		"id",
		"title"
	FROM "categories"`
	filterValues := make([]interface{}, 0)
	if req.Title != "" {
		query += `
		WHERE (LOWER("title") LIKE $1)`
		filterValues = append(filterValues, "%"+strings.ToLower(req.Title)+"%")
	}
	query += ";"
	category := make([]*appinfo.Category, 0)
	err := r.db.Select(&category, query, filterValues...)
	if err != nil {
		return nil, fmt.Errorf("select categories failed: %v", err)
	}
	return category, nil
}

func (r *appinforepository) InsertCategory(req []*appinfo.Category) error {
	query := `
	INSERT INTO "categories" (
		"title"
	)
	VALUES`

	tx, err := r.db.BeginTxx(context.Background(), nil)
	if err != nil {
		return err
	}
	valuesStack := make([]interface{}, 0)
	for i, cat := range req {
		valuesStack = append(valuesStack, cat.Title)
		if i != len(req)-1 {
			query += fmt.Sprintf(`($%d),`, i+1)
		} else {
			query += fmt.Sprintf(`($%d)`, i+1)
		}
	}
	query += `RETURNING "id"`
	rows, err := tx.QueryxContext(context.Background(), query, valuesStack...)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("insert category failed %v", err)
	}
	var i int
	for rows.Next() {
		err = rows.Scan(&req[i].Id)
		if err != nil {
			return fmt.Errorf("scan categories failed: %v", err)
		}
		i++
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}
	return nil
}

func (r *appinforepository) DeleteCategory(categoryId int) error {
	query := `DELETE FROM "categories" WHERE "id" = $1`
	_, err := r.db.ExecContext(context.Background(), query, categoryId)
	if err != nil {
		return fmt.Errorf("delete category failed: %v", err)

	}
	return nil

}
