package databases

import (
	"log"

	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/jmoiron/sqlx"
	"gitlab.com/mrponpon/eshop/config"
)

func DbConnect(cfg config.IDbconfig) *sqlx.DB {
	//connect
	db, err := sqlx.Connect(
		"pgx",
		cfg.Url(),
	)
	if err != nil {
		log.Fatalf("connect db failed: %v\n", err)
	}
	db.DB.SetMaxOpenConns(cfg.MaxConnections())
	return db
}
