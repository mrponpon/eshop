package main

import (
	_ "fmt"
	"os"

	"gitlab.com/mrponpon/eshop/config"
	"gitlab.com/mrponpon/eshop/modules/servers"
	"gitlab.com/mrponpon/eshop/pkg/databases"
)

func envPath() string {
	if len(os.Args) == 1 {
		return ".env"
	} else {
		return os.Args[1]
	}
}

func main() {
	cfg := config.LoadConfig(envPath())
	db := databases.DbConnect(cfg.Db())
	defer db.Close()
	servers.NewServer(cfg, db).Start()
}
